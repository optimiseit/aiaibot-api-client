## Usage

See [the official documentation](https://developer.aiaibot.com/) for more information.

### Composer
Add the repository to `composer.json`
```json
{
    "repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/optimiseit/aiaibot-api-client.git"
        }
    ]
}
```

Require the package
```shell
$ composer require optimiseit/aiaibot-api-client
```

### Register webhook
The package provides a cli tool to set the webhook url
```shell
$ vendor/bin/aiaibot-set-webhook <apiServer> <apiKey> <webhookUrl>
```

### Webhook

```php
use Optimise\aiaibot\Webhook\HtmlMessage;
use Optimise\aiaibot\Webhook\WebhookEvent;

$webhookEvent = WebhookEvent::createFromRequest($psr7Request);

$message = $webhookEvent->getMessage();

switch (get_class($message)) {
    case HtmlMessage::class:
        /** @var HtmlMessage $message */
        $content = $message->content;
        // do stuff
        break;

    // ...
}
```

### Api client

Create a bot session
```php
use Optimise\aiaibot\Api\Client;
use Optimise\aiaibot\Api\Configuration;

$configuration = new Configuration($apiServer, $apiKey);
$client = new Client($configuration);
$createSessionResult = $client->createSession(Client::CREATE_SESSION_TYPE_URL, 'https://example.com', $userId);

$sessionId = $createSessionResult->id;

// ...
```

Send messages into session
```php
use Optimise\aiaibot\Api\Client;
use Optimise\aiaibot\Api\Configuration;

$configuration = new Configuration($apiServer, $apiKey);
$client = new Client($configuration);

$sendMessageResult = $client->sendMessage($sessionId, Client::SEND_MESSAGE_TYPE_TEXT, 'hi');

$success = $sendMessageResult->received;

// ...
```
