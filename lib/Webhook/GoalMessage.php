<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class GoalMessage implements Message
{
    /** @var string */
    public $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['name'],
        );
    }
}
