<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class ButtonsMessage implements Message
{
    /** @var string[] */
    public $buttons;

    public function __construct(array $buttons)
    {
        $this->buttons = $buttons;
    }

    public static function fromPayload(array $payload): self
    {
        return new self($payload['buttons']);
    }
}
