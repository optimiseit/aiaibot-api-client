<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class FileUploadMessage implements Message
{
    public const ALLOWED_TYPE_ALL = 'all';
    public const ALLOWED_TYPE_IMAGE = 'image';
    public const ALLOWED_TYPE_DOCUMENT = 'document';
    public const ALLOWED_TYPE_VIDEO = 'video';

    /** @var integer */
    public $maxFiles;
    /** @var string[] */
    public $allowedTypes;

    public function __construct(int $maxFiles, array $allowedTypes)
    {
        $this->maxFiles = $maxFiles;
        $this->allowedTypes = $allowedTypes;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['maxFiles'],
            $payload['allowedTypes']
        );
    }
}
