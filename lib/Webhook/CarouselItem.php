<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class CarouselItem
{
    /** @var string */
    public $title;
    /** @var string */
    public $description;
    /** @var string */
    public $image;
    /** @var string */
    public $url;
    /** @var string */
    public $urlText;

    public function __construct(
        string $title,
        string $description,
        string $image,
        string $url,
        string $urlText
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->url = $url;
        $this->urlText = $urlText;
    }
}
