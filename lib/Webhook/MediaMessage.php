<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class MediaMessage implements Message
{
    public const MEDIA_TYPE_IMAGE = 'image';
    public const MEDIA_TYPE_LINK = 'link';
    public const MEDIA_TYPE_YOUTUBE = 'youtube';
    public const MEDIA_TYPE_VIMEO = 'vimeo';

    /** @var string */
    public $mediaType;
    /** @var string */
    public $url;
    /** @var string */
    public $description;

    public function __construct(string $mediaType, string $url, string $description)
    {
        $this->mediaType = $mediaType;
        $this->url = $url;
        $this->description = $description;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['mediaType'],
            $payload['url'],
            $payload['description']
        );
    }
}
