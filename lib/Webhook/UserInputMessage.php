<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class UserInputMessage implements Message
{
    public const INPUT_TYPE_TEXT = 'text';
    public const INPUT_TYPE_EMAIL = 'email';
    public const INPUT_TYPE_PHONE = 'phone';

    /** @var string */
    public $inputType;
    /** @var ?integer */
    public $maxLength;

    public function __construct(string $inputType, ?int $maxLength = null)
    {
        $this->inputType = $inputType;
        $this->maxLength = $maxLength;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['inputType'],
            (int)$payload['maxLength']
        );
    }
}
