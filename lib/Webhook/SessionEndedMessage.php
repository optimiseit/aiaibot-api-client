<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class SessionEndedMessage implements Message
{
    public static function fromPayload(array $payload): self
    {
        // no payload
        return new self();
    }
}
