<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

use InvalidArgumentException;
use Optimise\aiaibot\Exception\Exception;
use Psr\Http\Message\RequestInterface;

class WebhookEvent
{
    /** @var string */
    private $eventType;
    /** @var string */
    private $sessionId;
    /** @var Message */
    private $message;

    public function __construct(string $eventType, string $sessionId, Message $message)
    {
        $this->eventType = $eventType;
        $this->sessionId = $sessionId;
        $this->message = $message;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public static function createFromRequest(RequestInterface $request): self
    {
        $postData = (array)json_decode((string)$request->getBody(), true);

        if (empty($postData)) {
            throw new InvalidArgumentException("Invalid data sent to webhook");
        }

        $eventType = $postData['eventType'] ?? null;
        $sessionId = $postData['data']['sessionId'] ?? null;

        if ($sessionId === null || $sessionId === '') {
            throw new InvalidArgumentException("Invalid sessionId sent to webhook");
        }

        if ($eventType === 'session_ended') {
            $message = SessionEndedMessage::fromPayload([]);
            return new self($eventType, $sessionId, $message);
        }

        if ($eventType === 'message') {
            $messageType = $postData['data']['message']['payloadType'] ?? null;
            $messagePayload = $postData['data']['message']['payload'] ?? [];

            switch ($messageType) {
                case 'html':
                    $message = HtmlMessage::fromPayload($messagePayload);
                    break;
                case 'media':
                    $message = MediaMessage::fromPayload($messagePayload);
                    break;
                case 'button-group':
                    $message = ButtonsMessage::fromPayload($messagePayload);
                    break;
                case 'carousel':
                    $message = CarouselMessage::fromPayload($messagePayload);
                    break;
                case 'user-input':
                    $message = UserInputMessage::fromPayload($messagePayload);
                    break;
                case 'file-upload':
                    $message = FileUploadMessage::fromPayload($messagePayload);
                    break;
                case 'survey':
                    $message = SurveyMessage::fromPayload($messagePayload);
                    break;
                case 'intent':
                    $message = IntentMessage::fromPayload($messagePayload);
                    break;
                case 'live-chat':
                    $message = LiveChatMessage::fromPayload($messagePayload);
                    break;
                default:
                    throw new Exception("Unknown message type '{$messageType}'");
            }

            return new self($eventType, $sessionId, $message);
        }

        if ($eventType === 'info') {
            $messageType = $postData['data']['message']['payloadType'] ?? null;
            $messagePayload = $postData['data']['message']['payload'] ?? [];

            switch ($messageType) {
                case 'goal':
                    $message = GoalMessage::fromPayload($messagePayload);
                    break;
                default:
                    throw new Exception("Unknown info event type '{$messageType}'");
            }

            return new self($eventType, $sessionId, $message);
        }

        throw new InvalidArgumentException("Invalid event '$eventType' type sent to webhook");
    }
}
