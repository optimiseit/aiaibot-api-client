<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class LiveChatMessage implements Message
{
    /** @var string */
    public $provider;
    /** @var string */
    public $description;

    public function __construct(string $provider, string $description)
    {
        $this->provider = $provider;
        $this->description = $description;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['provider'],
            $payload['description']
        );
    }
}
