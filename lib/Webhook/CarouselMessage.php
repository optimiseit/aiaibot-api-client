<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class CarouselMessage implements Message
{
    /** @var string */
    public $name;
    public $button;
    public $items;

    public function __construct(string $name, string $button, array $items)
    {
        $this->name = $name;
        $this->button = $button;
        $this->items = $items;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['name'],
            $payload['button'],
            array_map(
                static function (array $item) {
                    return new CarouselItem(
                        $item['title'],
                        $item['description'],
                        $item['image'],
                        $item['url'],
                        $item['urlText']
                    );
                },
                $payload['items']
            )
        );
    }
}
