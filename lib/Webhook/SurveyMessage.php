<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class SurveyMessage implements Message
{
    public const SURVEY_TYPE_STARS = 'stars';
    public const SURVEY_TYPE_EMOJI = 'emoji';
    public const SURVEY_TYPE_EMOJI_3 = 'emoji-3';
    public const SURVEY_TYPE_NPS = 'nps';
    public const SURVEY_TYPE_CSAT = 'csat';

    /** @var string */
    public $surveyType;
    /** @var int[]|string[] */
    public $options;

    public function __construct(string $surveyType, array $options)
    {
        $this->surveyType = $surveyType;
        $this->options = $options;
    }

    public static function fromPayload(array $payload): self
    {
        return new self(
            $payload['surveyType'],
            $payload['options']
        );
    }
}
