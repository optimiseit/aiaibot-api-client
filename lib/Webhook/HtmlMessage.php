<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

final class HtmlMessage implements Message
{
    /** @var string */
    public $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public static function fromPayload(array $payload): self
    {
        return new self($payload['content']);
    }
}
