<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Webhook;

interface Message
{
    public static function fromPayload(array $payload);
}
