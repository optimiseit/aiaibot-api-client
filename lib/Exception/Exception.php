<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Exception;

use Exception as PhpException;
use Throwable;

class Exception extends PhpException
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct("[aiaibot] $message", $code, $previous);
    }
}
