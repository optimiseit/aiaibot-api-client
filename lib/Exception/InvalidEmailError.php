<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Exception;

class InvalidEmailError extends Exception
{
}
