<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

final class CreateSessionResult implements ApiResult
{
    /** @var string */
    public $id;
    /** @var string */
    public $userId;

    public function __construct(string $id, string $userid)
    {
        $this->id = $id;
        $this->userId = $userid;
    }

    public static function createFromData(array $data): self
    {
        return new self($data['id'], $data['userId']);
    }
}
