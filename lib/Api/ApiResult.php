<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

interface ApiResult
{
    public static function createFromData(array $data);
}
