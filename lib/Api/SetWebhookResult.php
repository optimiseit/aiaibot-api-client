<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

final class SetWebhookResult implements ApiResult
{
    /** @var string */
    public $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public static function createFromData(array $data): self
    {
        return new self($data['url']);
    }
}
