<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

final class SendMessageResult implements ApiResult
{
    /** @var bool */
    public $ok;

    public function __construct(bool $ok)
    {
        $this->ok = $ok;
    }

    public static function createFromData(array $data): self
    {
        return new self($data['ok']);
    }
}
