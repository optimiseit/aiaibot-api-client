<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Utils;
use InvalidArgumentException;
use JsonException;
use Optimise\aiaibot\Exception\Exception;
use Optimise\aiaibot\Exception\InvalidEmailError;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /** @var GuzzleClient */
    private $client;

    public const CREATE_SESSION_TYPE_URL = 'url';
    public const CREATE_SESSION_TYPE_STORY_ID = 'storyId';

    public const SEND_MESSAGE_TYPE_TEXT = 'text';
    public const SEND_MESSAGE_TYPE_FILES = 'files';

    /**
     * Client constructor.
     */
    public function __construct(Configuration $configuration)
    {
        // @see https://developer.aiaibot.com/#servers
        $this->client = new GuzzleClient(
            [
                'base_uri' => $configuration->apiServer,
                'headers' => [
                    'aiaibot-api-key' => $configuration->apiKey,
                ],
            ]
        );
    }

    /**
     * @throws Exception
     * @throws JsonException
     */
    private function postJson(string $route, array $data): ResponseInterface
    {
        try {
            return $this->client->post(
                $route,
                [
                    'json' => $data,
                ]
            );
        } catch (BadResponseException $e) {
            /** @var ResponseInterface $response */
            $response = $e->getResponse();

            $this->processErrorResponse($response);

            throw new Exception("Failed to query api ({$route}): {$response->getBody()}");
        }
    }

    /**
     * @throws InvalidEmailError
     * @throws JsonException
     */
    private function processErrorResponse(ResponseInterface $response): void
    {
        $responseBody = (string)$response->getBody();
        if ($responseBody === '') {
            return;
        }

        $responseObject = json_decode($responseBody, false, 512, JSON_THROW_ON_ERROR);
        if (!is_object($responseObject)) {
            return;
        }

        $error = $responseObject->error ?? null;
        if ($error === null) {
            return;
        }

        switch ($error->reason ?? null) {
            case 'error.validation_error':
                foreach ($error->errors ?? [] as $item) {
                    switch ($item->reason ?? null) {
                        case 'error.email.invalid':
                            throw new InvalidEmailError($item->message);
                    }
                }
        }
    }

    private function getDataFromResponse(ResponseInterface $response): array
    {
        return (array)json_decode((string)$response->getBody(), true);
    }

    /**
     * @see https://developer.aiaibot.com/#post-/messaging/v1/webhooks
     * @throws Exception
     * @throws JsonException
     */
    public function setWebhook(string $webhookUrl): SetWebhookResult
    {
        if ($webhookUrl === '') {
            throw new InvalidArgumentException('Parameter $webhookUrl requires a value');
        }

        $response = $this->postJson(
            '/messaging/v1/webhooks',
            [
                'url' => $webhookUrl,
            ]
        );

        $responseData = $this->getDataFromResponse($response);

        return SetWebhookResult::createFromData($responseData['data']);
    }

    /**
     * @throws Exception
     * @throws JsonException
     */
    public function createSession(string $identifierType, string $identifier, ?string $userId = null): CreateSessionResult
    {
        if (!in_array($identifierType, [self::CREATE_SESSION_TYPE_URL, self::CREATE_SESSION_TYPE_STORY_ID], true)) {
            throw new InvalidArgumentException('Invalid value for $identifierType');
        }
        if ($identifier === '') {
            throw new InvalidArgumentException('Parameter $identifier requires a value');
        }
        if ($userId === '') {
            throw new InvalidArgumentException('Parameter $userId requires a value, if given');
        }

        $requestData = [
            $identifierType => $identifier,
        ];

        if ($userId !== null) {
            $requestData['userId'] = $userId;
        }

        $response = $this->postJson('/messaging/v1/chatbot-sessions', $requestData);

        if ($response->getStatusCode() === 204) {
            throw new Exception('Url not allowed or story not published');
        }

        $responseData = $this->getDataFromResponse($response);

        return CreateSessionResult::createFromData($responseData['data']);
    }

    /**
     * @see https://developer.aiaibot.com/#post-/messaging/v1/chatbot-sessions/-id-/message
     * @param string|string[] $content Text or list of file UUIDs
     * @throws Exception
     * @throws JsonException
     */
    public function sendMessage(string $sessionId, string $type, $content): SendMessageResult
    {
        if ($sessionId === '') {
            throw new InvalidArgumentException('Parameter $sessionId requires a value');
        }
        if (!in_array($type, [self::SEND_MESSAGE_TYPE_TEXT, self::SEND_MESSAGE_TYPE_FILES], true)) {
            throw new InvalidArgumentException('Invalid value for $type');
        }
        if (!is_string($content) && !is_array($content)) {
            throw new InvalidArgumentException('Parameter $content requires a value');
        }
        if (is_array($content) && !count($content)) {
            throw new InvalidArgumentException('Parameter $content requires at least one record, if list of files');
        }
        if (is_array($content)) {
            foreach ($content as $item) {
                if (!is_string($item) || $item === '') {
                    throw new InvalidArgumentException('Parameter $content can only contain strings, if list of files');
                }
            }
        }

        $response = $this->postJson(
            "/messaging/v1/chatbot-sessions/{$sessionId}/message",
            [
                'sessionId' => $sessionId,
                'type' => $type,
                'payload' => [
                    'text' => $content,
                ],
            ]
        );

        $responseData = $this->getDataFromResponse($response);

        return SendMessageResult::createFromData($responseData['data']);
    }

    /**
     * @see https://developer.aiaibot.com/#post-/messaging/v1/chatbot-sessions/-id-/file-upload
     * @param string|resource $file File resource or path to upload data
     */
    public function uploadFile(string $sessionId, string $fileName, $file): UploadFileResult
    {
        if ($sessionId === '') {
            throw new InvalidArgumentException('Parameter $sessionId requires a value');
        }
        if ($fileName === '') {
            throw new InvalidArgumentException('Parameter $fileName requires a value');
        }
        if (
            $file === ''
            || (is_resource($file) && get_resource_type($file) !== 'stream')
            || (is_string($file) && !is_readable($file))
        ) {
            throw new InvalidArgumentException('Parameter $sessionId requires a readable file');
        }

        $fp = is_resource($file) ? $file : Utils::tryFopen($file, 'r');

        $response = $this->client->post(
            "/messaging/v1/chatbot-sessions/{$sessionId}/file-upload",
            [
                'multipart' => [
                    'name' => 'file',
                    'contents' => $fp,
                    'filename' => $fileName,
                ],
            ]
        );

        // if the file is opened by us, we should close it
        if (!is_resource($file)) {
            fclose($fp);
        }

        $responseData = $this->getDataFromResponse($response);

        return UploadFileResult::createFromData($responseData['data']);
    }
}
