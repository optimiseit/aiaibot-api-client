<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

use InvalidArgumentException;

final class Configuration
{
    /** @var string */
    public $apiServer;
    /** @var string */
    public $apiKey;

    public function __construct(
        string $apiServer,
        string $apiKey
    ) {
        if ($apiServer === '') {
            throw new InvalidArgumentException('Parameter $apiServer requires a value');
        }
        if ($apiKey === '') {
            throw new InvalidArgumentException('Parameter $apiKey requires a value');
        }

        $this->apiServer = $apiServer;
        $this->apiKey = $apiKey;
    }
}
