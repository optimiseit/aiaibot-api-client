<?php

declare(strict_types=1);

namespace Optimise\aiaibot\Api;

final class UploadFileResult implements ApiResult
{
    /** @var string */
    public $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function createFromData(array $data): self
    {
        return new self($data['id']);
    }
}
