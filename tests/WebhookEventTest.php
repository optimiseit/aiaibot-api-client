<?php

declare(strict_types=1);

use GuzzleHttp\Psr7\Request;
use Optimise\aiaibot\Exception\Exception;
use Optimise\aiaibot\Webhook\ButtonsMessage;
use Optimise\aiaibot\Webhook\CarouselItem;
use Optimise\aiaibot\Webhook\CarouselMessage;
use Optimise\aiaibot\Webhook\FileUploadMessage;
use Optimise\aiaibot\Webhook\GoalMessage;
use Optimise\aiaibot\Webhook\HtmlMessage;
use Optimise\aiaibot\Webhook\IntentMessage;
use Optimise\aiaibot\Webhook\LiveChatMessage;
use Optimise\aiaibot\Webhook\MediaMessage;
use Optimise\aiaibot\Webhook\SessionEndedMessage;
use Optimise\aiaibot\Webhook\SurveyMessage;
use Optimise\aiaibot\Webhook\UserInputMessage;
use Optimise\aiaibot\Webhook\WebhookEvent;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

class WebhookEventTest extends TestCase
{
    private $sessionId = 'dummySessionId';

    private function createPsr7WebhookRequestWithPostData(array $postData): RequestInterface
    {
        return new Request(
            'POST',
            '/webhook',
            [
                'Content-Type' => 'application/json',
            ],
            json_encode($postData)
        );
    }

    public function testHtmlMessage(): void
    {
        $content = '<p>test</p>';

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'html',
                    'payload' => [
                        'content' => $content,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var HtmlMessage $htmlMessage */
        $htmlMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(HtmlMessage::class, $htmlMessage);
        self::assertEquals($content, $htmlMessage->content);
    }

    public function testMediaMessage(): void
    {
        $mediaType = MediaMessage::MEDIA_TYPE_YOUTUBE;
        $url = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';
        $description = 'Amazing!';

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'media',
                    'payload' => [
                        'mediaType' => $mediaType,
                        'url' => $url,
                        'description' => $description,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var MediaMessage $mediaMessage */
        $mediaMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(MediaMessage::class, $webhookEvent->getMessage());
        self::assertEquals($mediaType, $mediaMessage->mediaType);
        self::assertEquals($url, $mediaMessage->url);
        self::assertEquals($description, $mediaMessage->description);
    }

    public function testButtonsMessage(): void
    {
        $buttons = ['Good', 'Bad'];

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'button-group',
                    'payload' => [
                        'buttons' => $buttons,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var ButtonsMessage $buttonsMessage */
        $buttonsMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(ButtonsMessage::class, $buttonsMessage);
        self::assertEquals($buttons, $buttonsMessage->buttons);
    }

    public function testCarouselMessage(): void
    {
        $name = 'Carousel name';
        $button = 'Pick this one!';
        $itemTitle = 'option 1';
        $itemDescription = 'random text';
        $itemImage = 'https://does.not.exists/image.png';
        $itemUrl = 'https://does.not.exists';
        $itemUrlText = 'does.not.exists';

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'carousel',
                    'payload' => [
                        'name' => $name,
                        'button' => $button,
                        'items' => [
                            [
                                'title' => $itemTitle,
                                'description' => $itemDescription,
                                'image' => $itemImage,
                                'url' => $itemUrl,
                                'urlText' => $itemUrlText,
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var CarouselMessage $carouselMessage */
        $carouselMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(CarouselMessage::class, $carouselMessage);
        self::assertEquals($name, $carouselMessage->name);
        self::assertEquals($button, $carouselMessage->button);

        self::assertIsArray($carouselMessage->items);
        self::assertCount(1, $carouselMessage->items);

        /** @var CarouselItem $item */
        $item = $carouselMessage->items[0];

        self::assertInstanceOf(CarouselItem::class, $item);
        self::assertEquals($itemTitle, $item->title);
        self::assertEquals($itemDescription, $item->description);
        self::assertEquals($itemImage, $item->image);
        self::assertEquals($itemUrl, $item->url);
        self::assertEquals($itemUrlText, $item->urlText);
    }

    public function testUserInputMessage(): void
    {
        $inputType = UserInputMessage::INPUT_TYPE_TEXT;
        $maxLength = 32;

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'user-input',
                    'payload' => [
                        'inputType' => $inputType,
                        'maxLength' => $maxLength,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var UserInputMessage $userInputMessage */
        $userInputMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(UserInputMessage::class, $userInputMessage);
        self::assertEquals($inputType, $userInputMessage->inputType);
        self::assertEquals($maxLength, $userInputMessage->maxLength);
    }

    public function testFileUploadMessage(): void
    {
        $maxFiles = 1;
        $allowedTypes = [FileUploadMessage::ALLOWED_TYPE_ALL];

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'file-upload',
                    'payload' => [
                        'maxFiles' => $maxFiles,
                        'allowedTypes' => $allowedTypes,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var FileUploadMessage $fileUploadMessage */
        $fileUploadMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(FileUploadMessage::class, $fileUploadMessage);
        self::assertEquals($maxFiles, $fileUploadMessage->maxFiles);
        self::assertEquals($allowedTypes, $fileUploadMessage->allowedTypes);
    }

    public function testSurveyMessage(): void
    {
        $surveyType = SurveyMessage::SURVEY_TYPE_EMOJI;
        $options = [1, 2, 3, 4, 5];

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'survey',
                    'payload' => [
                        'surveyType' => $surveyType,
                        'options' => $options,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var SurveyMessage $surveyMessage */
        $surveyMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(SurveyMessage::class, $surveyMessage);
        self::assertEquals($surveyType, $surveyMessage->surveyType);
        self::assertEquals($options, $surveyMessage->options);
    }

    public function testIntentMessage(): void
    {
        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'intent',
                    'payload' => [
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());
        self::assertInstanceOf(IntentMessage::class, $webhookEvent->getMessage());
    }

    public function testLiveChatMessage(): void
    {
        $provider = 'aiaibot-api';
        $description = 'Amazing!';

        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'live-chat',
                    'payload' => [
                        'provider' => $provider,
                        'description' => $description,
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var LiveChatMessage $liveChatMessage */
        $liveChatMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(LiveChatMessage::class, $liveChatMessage);
        self::assertEquals($provider, $liveChatMessage->provider);
        self::assertEquals($description, $liveChatMessage->description);
    }

    public function testSessionEndedMessage(): void
    {
        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'session_ended',
            'data' => [
                'sessionId' => $this->sessionId,
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var SessionEndedMessage $sessionEndedMessage */
        $sessionEndedMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(SessionEndedMessage::class, $sessionEndedMessage);
    }

    public function testGoalMessage(): void
    {
        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'info',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'goal',
                    'payload' => [
                        'name' => 'test',
                    ],
                ],
            ],
        ]);

        $webhookEvent = WebhookEvent::createFromRequest($request);

        self::assertEquals($this->sessionId, $webhookEvent->getSessionId());

        /** @var GoalMessage $goalMessage */
        $goalMessage = $webhookEvent->getMessage();

        self::assertInstanceOf(GoalMessage::class, $goalMessage);
        self::assertEquals('test', $goalMessage->name);
    }

    public function testInvalidWebhookRequestEmptyBody(): void
    {
        $this->expectException(InvalidArgumentException::class);
        WebhookEvent::createFromRequest($this->createPsr7WebhookRequestWithPostData([]));
    }

    public function testInvalidWebhookRequestWrongEventType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        WebhookEvent::createFromRequest($this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'invalid',
        ]));
    }

    public function testInvalidWebhookRequestWrongMissingSessionId(): void
    {
        $this->expectException(InvalidArgumentException::class);
        WebhookEvent::createFromRequest($this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
        ]));
    }

    public function testInvalidMessageType(): void
    {
        $request = $this->createPsr7WebhookRequestWithPostData([
            'eventType' => 'message',
            'data' => [
                'sessionId' => $this->sessionId,
                'message' => [
                    'payloadType' => 'invalid',
                    'payload' => [
                    ],
                ],
            ],
        ]);

        $this->expectException(Exception::class);
        WebhookEvent::createFromRequest($request);
    }
}
